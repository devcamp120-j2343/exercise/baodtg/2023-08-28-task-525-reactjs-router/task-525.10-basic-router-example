import { Routes, Route } from "react-router-dom";
import routes from "../routes";

const Body = () => {
    return (
        <div style={{backgroundColor: "red"}}>
            <Routes>
                {
                    routes.map((route, index) => {
                        return <Route path={route.path} element={route.element}/>
                    })
                }
            </Routes>
        </div>
    )
}

export default Body;