import NotFound from "./pages/404";
import FirstPage from "./pages/FirstPage";
import HomePage from "./pages/HomePage";
import SecondPage from "./pages/SecondPage";
import ThirdPage from "./pages/ThirdPage";

const routes = [
    {
        path: "/",
        element: <HomePage />
    },
    {
        path: "/firstpage",
        element: <FirstPage />
    },
    {
        path: "/secondpage/:productId",
        element: <SecondPage />
    },
    {
        path: "/secondpage/:productId/:typeId",
        element: <SecondPage />
    },
    {
        path: "/thirdpage",
        element: <ThirdPage />
    },
    {
        path: "*",
        element: <NotFound />
    },
];

export default routes;