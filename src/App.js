import Body from "./views/Body";
import Footer from "./views/Footer";
import Header from "./views/Header";

function App() {
  return (
    <div>
      <Header />
      <Body />
      <Footer />
    </div>
  );
}

export default App;
